# Mac-specific Terminal Commands

## Show All Apps Downloaded from the Mac App Store
```
find /Applications -path '*Contents/_MASReceipt/receipt' -maxdepth 4 -print |\sed 's#.app/Contents/_MASReceipt/receipt#.app#g; s#/Applications/##' > appstorelist.txt
```

## Show Hidden Files in Finder
```
defaults write com.apple.finder AppleShowAllFiles TRUE
```

## Change the File Format for Screenshots
```
defaults write com.apple.screencapture type file-extension
```

## List All Available Mac Software Updates from Command Line
```
softwareupdate -l
```

## Installing All Available Updates from Terminal
```
sudo softwareupdate -iva
```

## Install Recommended Updates Only from Terminal in OS X
```
sudo softwareupdate -irv
```

## Installing & Ignoring Specific Software Updates to Mac from Terminal of OS X
```
sudo softwareupdate -i iPhoneConfigurationUtility-3.2
```

## Show available Shortcuts
```
softwareupdate -h
```

## Processes

### Show all processes
```
ps -ax
```

### Show processes by name
```
ps -ax | grep <application name>
```

### Kill processes
 - get process PID
 - use terminal to do so 
 ```
 kill 14530
 ```   

 ### Kill all processes based on the name (be careful here!)
 ```
 killall Skype
 ```
# Progress terminal addon

## Installation: 
```
git clone  https://github.com/Xfennec/progress.git
cd progress
make 
sudo make install
progress
```
## Usage
To display estimated I/O throughput and estimated remaining time for on going coreutils commands, enable the  -w option:
```
progress -w
```
To watch what is currently running
```
watch progress -q
```
To execute something and measure it:
```
tar czf images.tar.gz linuxmint-18-cinnamon-64bit.iso CentOS-7.0-1406-x86_64-DVD.iso CubLinux-1.0RC-amd64.iso | progress  -m  $!
```

# make bash script executable
```
chmod a+x script_name.sh
```   


